class CreateStoreProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :store_products do |t|
      t.string :store_number
      t.string :store_name
      t.string :store_details
      t.string :store_address
      t.string :availablity
      t.string :product_number
      t.string :product_name
 
      t.timestamps
    end
  end
end
