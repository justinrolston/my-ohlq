require 'sinatra'
require 'sinatra/base'
require_relative 'lib/ohlq'
require_relative 'lib/helper'
require 'pry'
require 'dalli'

require 'sinatra/activerecord'
require 'active_record/connection_adapters/postgresql_adapter'

class App < Sinatra::Base
  set :port, ENV["PORT"]
  set :bind, '0.0.0.0'
  set :public_folder, File.dirname(__FILE__) + '/content'
  set :cache, Helper.get_app_cache_connection
  settings.cache.flush
  set :show_exceptions, :after_handler

  get '/' do
    erb :home, :layout => :main
  end

  get '/:name' do |name|
    @name = params['name']
    unless settings.cache.get("#{name}-results-data")
      products = Ohlq.get_products_from_file @name 
      stores = Ohlq.get_stores_from_file @name 
      @stores = Ohlq.find_products(stores,products)
      settings.cache.set("#{name}-results-data", @stores)
    else
      @stores = settings.cache.get("#{name}-results-data")
    end
    erb :user_home, :layout => :main
  end

  get '/:name/stores' do |name|
    @name = params['name']
    unless settings.cache.get("#{name}-stores-data")
      stores = Ohlq.get_stores_from_file params['name']
      @stores = Ohlq.get_stores_info(stores)
      settings.cache.set("#{name}-stores-data", @stores)
    else
      @stores = settings.cache.get("#{name}-stores-data")
    end
    erb :stores, :layout => :main
  end

  get '/:name/products' do |name|
    @name = params['name']
    unless settings.cache.get("#{name}-products-data")
      products = Ohlq.get_products_from_file params['name']
      @products = Ohlq.get_product_names(products)
      settings.cache.set("#{name}-products-data", @products)
    else
      @products = settings.cache.get("#{name}-products-data")
    end
    @products.sort_by! { |ps| ps[:name] }
    erb :products, :layout => :main
  end

  get '/flush' do
    settings.cache.flush
    redirect "/"
  end

  error 400..510 do
    erb :error
  end

  run! if app_file == $0
end