FROM ruby:2.5


WORKDIR /app

COPY Gemfile /app/

RUN bundle 

COPY . /app

ENTRYPOINT ["./migrate"]

# Matches port in app.rb
CMD ["ruby", "app.rb"]