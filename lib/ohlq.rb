require 'mechanize'
require 'pry'

class Ohlq

    def self.get_products_from_file file_name
         IO.readlines("data/products/#{file_name}").map(&:chomp)
    end

    def self.get_stores_from_file file_name
        IO.readlines("data/stores/#{file_name}").map(&:chomp)
    end

    def self.find_products(stores, products)
        agent = Mechanize.new
        agent.verify_mode= OpenSSL::SSL::VERIFY_NONE
        story_results = Array.new
        stores.each do |store|
            page = agent.get("https://www.ohlq.com/store/#{store}")
            store_details = page.css('.widget-header').children[1].inner_text
            products_found = Array.new       
            page.css('#theDatatable tr').each_entry do |d|
                products.each do |product|
                    product_number = d.children[0].children.text
                    product_name = d.children[1].children.text
                    if product_number == product && instock?(d.children[5])
                        products_found << {id: product_number, 
                            name: product_name,
                            link: "https://www.ohlq.com/store/#{product_number}"
                        }
                    end             
                end          
            end
            
            if products_found.count > 0
                story_results << {id: store,
                    name: page.css("#AgencyName").text,
                    address: page.css(".AgencyAddress").text, 
                    link: "https://www.ohlq.com/store/#{store}",
                    products: products_found,
                    availablity: get_availablity(page.css("div.widget-header.redTableHdr").text)
                }
            end  
        end
        story_results 
    end

    def self.get_availablity(availablity)
        start = availablity.index("as of ") + 6
        end_of_line = availablity.length - 8
        time_string = availablity[start..end_of_line]
        time_string
    end

    def self.instock? tr_product_row
        #puts tr_product_row.to_s
        if tr_product_row.name != "td"
            return false
        end

        unless tr_product_row.child != nil 
            return false
        end
        if tr_product_row.children[0] == nil or tr_product_row.children[0].attributes == nil
            return false 
        end
        return (tr_product_row.children[0].attributes["alt"].value == "In Stock" or 
            tr_product_row.children[0].attributes["alt"].value == "Limited Supply")
    end

    def self.record_products_to_spreadsheet(products)
    end

    def self.get_product_names products
        agent = Mechanize.new
        agent.verify_mode= OpenSSL::SSL::VERIFY_NONE
        results = Array.new
        products.each do |product|
            page = agent.get("https://www.ohlq.com/product/#{product}")
            results << {id: product, name: page.css('#AgencyName').children.text, 
                link: "https://www.ohlq.com/product/#{product}",
                message: page.css('.container h5').text.to_s}
        end
        results 
    end

    def self.get_stores_info stores
        agent = Mechanize.new
        agent.verify_mode= OpenSSL::SSL::VERIFY_NONE
        results = Array.new
        stores.each do |store|
            page = agent.get("https://www.ohlq.com/store/#{store}")
            #require 'pry'; binding.pry
            results << {id: store,
                name: page.css("#AgencyName").text,
                address: page.css(".AgencyAddress").text, 
                link: "https://www.ohlq.com/store/#{store}"
            }
        end
        results 
    end

end