require 'dalli'

class Helper

    def self.get_app_cache_connection
        options = { :namespace => "app", :expires_in => 3600, :compress => true }
        if ENV["MEMCACHEDCLOUD_SERVERS"]
          return Dalli::Client.new(ENV["MEMCACHEDCLOUD_SERVERS"].split(','), 
            :username => ENV["MEMCACHEDCLOUD_USERNAME"],
            :password => ENV["MEMCACHEDCLOUD_PASSWORD"], 
            :expires_in => 3600)
        else
          return Dalli::Client.new('cache', options )
          #return Dalli::Client.new('127.0.0.1', options )
        end
        #set :cache, Dalli::Client.new
    end

end