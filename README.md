# my-ohlq

### How to add yourself to the site:

1.  Create a branch
2.  Create a product file in `data/products/justin` with your name instead of justin and added OHLQ Product numbers
3.  Create a store file in `data/stores/justin` with your name instead of justin and added OHLQ Store numbers
4.  Create a Pull Request
5.  I'll check it and merge it
6.  Once this is done, it will automatically deploy 
7.  Then you'll be able to go to https://my-ohlq.herokuapp.com/justin but with your name instead of justin
